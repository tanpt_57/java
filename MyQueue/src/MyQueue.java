import java.util.NoSuchElementException;

public class MyQueue{
	
	private Node head;
	private Node tail;
	
	public MyQueue() {
		head = null;
		tail = null;
	}

	public void clear() {
		// TODO Auto-generated method stub
		if (head == null) {
			return;
		}
		
		head = null;
		tail = null;
	}

	public boolean contains(String arg0) {
		// TODO Auto-generated method stub
		if (head != null) {
			Node cur = head;
			while (cur != null) {
				if (cur.data.equals(arg0))
					return true;
				cur = cur.next;
			}
		}
		return false;
	}

	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if (head == null)
			return true;
		return false;
	}

	public int size() {
		// TODO Auto-generated method stub
		int size = 0;
		Node cur = head;
		while (cur != null) {
			size++;
			cur = cur.next;
		}
		return size;
	}

	public String[] toArray() {
		// TODO Auto-generated method stub
		String[] arr = new String[size()];
		int i = 0;
		Node cur = head;
		while (cur != null) {
			arr[i++] = cur.data;
			cur = cur.next;
		}
		
		return arr;
	}

	public synchronized boolean add(String e) {
		// TODO Auto-generated method stub
		if (e == null)
			return false;
		
		if (head == null) {
			head = new Node(e);
			tail = head;
			return true;
		}
		
		if (head != null) {
			Node node = new Node(e);
			Node tmp = tail;
			tail = node;
			tmp.next = tail;
			return true;
		}
		
		throw new IllegalStateException();
	}

	public String element() {
		// TODO Auto-generated method stub
		if (head != null) {
			return head.data;
		}
		else {
			throw new NoSuchElementException();
		}
	}

	public synchronized boolean offer(String e) {
		// TODO Auto-generated method stub
		if (e == null)
			return false;
		
		if (head == null) {
			head = new Node(e);
			tail = head;
			return true;
		}
		
		if (head != null) {
			Node node = new Node(e);
			Node tmp = tail;
			tail = node;
			tmp.next = tail;
			return true;
		}
		
		return false;
	}

	public String peek() {
		// TODO Auto-generated method stub
		if (head != null) {
			return head.data;
		}
		return null;
	}

	public synchronized String poll() {
		// TODO Auto-generated method stub
		if (head != null) {
			Node tmp = head;
			head = null;
			head = tmp.next;
			
			return tmp.data;
		}
		return null;
	}

	public synchronized String remove() {
		// TODO Auto-generated method stub
		if (head != null) {
			Node tmp = head;
			head = null;
			head = tmp.next;
			
			return tmp.data;
		}
		
		throw new NoSuchElementException();
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder builder = new StringBuilder();
		Node cur = head;
		while (cur != null) {
			builder.append(cur.data + " ");
			cur = cur.next;
		}
		return builder.toString();
	}
	
	class Node {
		String data;
		Node next;
		
		public Node() {
			this.data = null;
			this.next = null;
		}
		
		public Node(String data) {
			this.data = data;
			this.next = null;
		}
	}

}
