import java.util.PriorityQueue;

public class Test {
	
	public static void main(String[] args) {
		
		MyQueue queue = new MyQueue();
		queue.add("1");
		queue.add("2");
		queue.add("3");
		queue.offer("4");
		queue.offer("5");
		queue.offer("6");
		
		System.out.println(queue.toString());
		
		System.out.println("size: " + queue.size());
		
		queue.remove();
		queue.poll();
		
		System.out.println(queue.toString());
		
		PriorityQueue<String> priorityQueue = new PriorityQueue<String>();
		
		long start = System.currentTimeMillis();
		
		int n = 100000;
		while (n-- > 0)
			queue.add("1");
		
		long total = System.currentTimeMillis()-start;
		System.out.println("My queue: "+ total);
		
		start = System.currentTimeMillis();
		
		n = 100000;
		while (n-- > 0)
			priorityQueue.add("1");
		
		total = System.currentTimeMillis()-start;
		System.out.println("Priority Queue: "+ total);
	}
}
